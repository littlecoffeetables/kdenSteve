#dungeon_simple

import curses #for colours

"""
The simplest dungeon generator I can think of
split the dungeon up into 12x12 areas, put a room in the middle of each
Then connect them up with straight 2-wide paths
"""

area_size = 12

def generate(EM, EQ, width, height, prefs={}):
	max_x = width//area_size
	max_y = height//area_size
	for x in range(max_x):
		for y in range(max_y):
			is_edge = (
					x == 0, x == max_x-1,
					y == 0, y == max_y-1)
			room_size = 2 * EM.randint(2, 5, "dungeon_rng")
			fill_in_area(EM, x, y, is_edge, room_size)
			pos = ( int((x+0.5)*area_size),
				int((y+0.5)*area_size))
			if x==y==1:
				EM.quick_entity("player", pos)
			elif x in (2,3) and y in (2,3):
				EM.quick_entity("orc", pos)
			elif x + y == 3:
				EM.quick_entity("cardboard_box", pos)
	min_x = max_x * area_size
	min_y = max_y * area_size
	for x in range(min_x, width):
		for y in range(0, height):
			create_tile(EM, (x,y), "wall")
	for x in range(0, min_x):
		for y in range(min_y, height):
			create_tile(EM, (x,y), "wall")


def fill_in_area(EM, ax,ay, is_edge, room_size):
	room_edge = (area_size - room_size)//2
	room = (room_edge, area_size - room_edge)
	corridor = (area_size//2 -1, area_size//2)
	flooded = EM.randint(0,5) == 0
	has_lava = EM.randint(0,2) == 0
	for x in range(area_size):
		for y in range(area_size):
			if (x >= room[0] and x<room[1]
					and y>=room[0] and y<room[1]):
				#create room
				mat = "water" if flooded else "floor"
				if mat == "floor" and has_lava:
					if EM.randint(0,5) == 0:
						mat = "lava"
				create_tile_in(EM, ax,ay,x,y, mat)
				continue
			less_half = (x < area_size//2, y < area_size//2)
			if (
				(x in corridor and not (
					(is_edge[2] and less_half[1])
					or (is_edge[3] and not less_half[1])
				))
			  or
				(y in corridor and not (
					(is_edge[0] and less_half[0])
					or (is_edge[1] and not less_half[0])
				))
			  ):
				#create corridor
				create_tile_in(EM, ax,ay,x,y, "floor")
				continue
			#if x==0 or y==0 or x==area_size-1 or y==area_size-1:
			#	#create wall
			#	create_tile_in(EM, ax,ay,x,y, "wall")
			#	continue
			#else create wall
			create_tile_in(EM, ax,ay,x,y, "wall")

def create_tile_in(EM, ax,ay,x,y, mat):
	pos = (ax*area_size +x, ay*area_size +y)
	create_tile(EM, pos, mat)

def create_tile(EM, pos, mat):
	if mat == "wall":
		EM.create_tile(pos, {"opaque":True,
			"r_char":"#", "r_prio":0, "physical":True,
			"r_colour": curses.COLOR_BLACK,
			"bg_colour": curses.COLOR_WHITE,
			})
	elif mat == "floor":
		EM.create_tile(pos, {
			"r_char":".", "r_prio":0,
			"r_colour": curses.COLOR_BLACK,
			"bg_colour": curses.COLOR_WHITE,
			})
	elif mat == "water":
		EM.create_tile(pos, {
			"r_char":".", "r_prio":0,
			"r_colour": curses.COLOR_BLACK,
			"bg_colour": curses.COLOR_WHITE,
			"watery": True,
			"temperature": 20.0,
			})
	elif mat == "lava":
		EM.create_tile(pos, {
			"r_char":".", "r_prio":0,
			"r_colour": curses.COLOR_YELLOW,
			"bg_colour": curses.COLOR_YELLOW,
			"lava": True,
			"temperature": 1200.0,
			})
