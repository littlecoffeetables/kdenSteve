#system_corpses

class Corpses(object):
	def __init__(self):
		pass

	def update(self, EM, EQ):
		maybe_new_corpses = EM.get_by_props(["p_pos", "health"])
		for e in maybe_new_corpses:
			if EM.get_value(e, "health") > 0:
				continue

			pos = EM.get_value(e, "p_pos")
			if EM.has_prop(e, "contents"):
				for other in EM.get_value(e, "contents"):
					EQ.schedule("exit", {
						"eid":other, "dir":(0,0)})
			if EM.has_prop(e, "blood"):
				EM.set_value(pos, "bloody", True)

			if EM.has_prop(e, "gibbed"):
				EM.rm_entity(e)
			else:
				EM.set_value(e, "r_char", "%")
				EM.edit_value(e, "r_prio", multi=0.04)
				EM.rm_value(e, "health")
				EM.rm_values(e, [
					"physical", "ai", "ap", "max_ap",
					"controlled", "opaque",
					], noerror=True)
			EQ.put("dead", {"eid":e, "pos":pos})
			EQ.put("refresh", {})


