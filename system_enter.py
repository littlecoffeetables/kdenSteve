#system_enter


class Enter(object):
	def __init__(self):
		pass

	def update(self, EM, EQ):
		i = 0
		while EQ.can_get("enter", i):
			event = EQ.get("enter", i)
			i += 1
			self.try_enter(EM, EQ, event, exit=False)
		i = 0
		while EQ.can_get("exit", i):
			event = EQ.get("exit", i)
			i += 1
			self.try_enter(EM, EQ, event, exit=True)
	
	def try_enter(self, EM, EQ, event, exit):
		eid = event["eid"]
		if not EM.has_prop(eid, "p_pos"):
			return False
		pos = EM.get_value(eid, "p_pos")
		if "dir" in event:
			new_pos = (pos[0]+event["dir"][0],
				   pos[1]+event["dir"][1])
		elif "to" in event:
			new_pos = event["to"]
		else:
			raise ValueError("event has no dir or to: "+str(event))

		if not exit:
			other = None
			others = EM.get_by_props(
				["enterable", "p_pos"], no=["full"])
			for o in others:
				if EM.get_value(o, "p_pos") != new_pos:
					continue
				if o == eid:
					#no entering into yourself
					continue
				other = o
				break
			else:
				return False

			self.do_enter(EM, EQ, eid, other)
		else:
			if not EM.has_prop(eid, "inside"):
				return False
			self.do_exit(EM, EQ, eid, new_pos)

		return True
	

	def do_enter(self, EM, EQ, eid, other):
		pos = EM.get_value(other, "p_pos")
		EM.set_value(eid, "p_pos", pos)
		EM.set_value(eid, "inside", other)
		contents_list = EM.get_value(other, "contents", default=[])
		contents_list.append(eid)
		EM.set_value(other, "contents", contents_list)
		if EM.get_value(other, "controllable_from_within", False):
			ctrl_list = EM.get_value(eid, "controlling", default=[])
			ctrl_list.append(other)
			EM.set_value(eid, "controlling", ctrl_list)
		
	def do_exit(self, EM, EQ, eid, new_pos):
		#EM.set_value(eid, "p_pos", new_pos)
		EQ.put("move", {"eid":eid, "to":new_pos})
		other = EM.get_value(eid, "inside")
		EM.rm_value(eid, "inside")
		#this will error if other for some reason doesn't contain eid
		EM.get_value(other, "contents").remove(eid)
		ctrl_list = EM.get_value(eid, "controlling")
		if other in ctrl_list:
			ctrl_list.remove(other)

