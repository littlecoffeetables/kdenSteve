#system_bathing

class Bathing(object):
	def __init__(self):
		pass

	def update(self, EM, EQ):
		if not (EQ.can_get("move",0)):
			return
		for w in EM.get_by_props(["watery", "p_pos"]):
			pos = EM.get_value(w, "p_pos")
			for e in EM.get_all_at_pos(pos):
				EM.rm_value(e, "bloody", noerror=True)
				EM.set_value(e, "wetness", 1.0)
				if EM.has_props(e, ["water_prone","health"]):
					EM.set_value(e, "health", 0)
