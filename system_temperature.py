#system_temperature

import utils

#these should maybe not be hardcoded
EVAP_RATE =		0.25
CONDUCT_RATE =		0.03
CONDUCT_RATE_ADJ =	0.0005

class Temperature(object):
	def __init__(self):
		pass

	def update(self, EM, EQ):
		for e in EM.get_by_props(
				["temperature", "p_pos", "ap_updated"]
			):
			#maybe also check adjacent tiles
			max_ap = EM.get_value(e, "max_ap", 1)
			pos = EM.get_value(e, "p_pos")
			self.transfer_heat(EM, EQ, e, pos, CONDUCT_RATE/max_ap)
			rate_adj = CONDUCT_RATE_ADJ / max_ap
			for adj_pos in utils.get_adjacent(pos):
				self.transfer_heat(
					EM, EQ, e, adj_pos, rate_adj)

			wet = EM.get_value(e, "wetness")
			if wet is not None and wet > 0:
				#technically water can't evaporate while
				# you're submerged but idk if I care
				tmp = EM.get_value(e, "temperature")
				evap_r = tmp/100 * EVAP_RATE
				to_evapor = max(evap_r*wet, 0)
				EM.edit_value(e, "wetness", add=-to_evapor)
				EM.edit_value(e, "temperature", add=-to_evapor)

	
	def transfer_heat(self, EM, EQ, e1, e2, rate):
		#this assumes we know e1 has a temp but e2 might not
		e2_temp = EM.get_value(e2, "temperature")
		if e2_temp is not None:
			e1_temp = EM.get_value(e1, "temperature")
			heat_diff = e2_temp - e1_temp
			heat_delta = rate * heat_diff
			EM.edit_value(e1, "temperature", add=heat_delta)
			EM.edit_value(e2, "temperature", add=-heat_delta)
