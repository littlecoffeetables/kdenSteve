import entities
import systems
#import event_queue
#import input_handler
#import event_handler
import main_screen
#import dungeon

import curses
#import time

def main(stdscr):
	stdscr.clear()
	curses.curs_set(False)

	#init colour pairs
	for fg in range(8):
		for bg in range(8):
			curses.init_pair(1 + 8 * fg + bg, fg, bg)

	f = open("debug.log", "w")
	f.close()

	EM = entities.EntityManager()

	d_size = (100,100)

	MS = main_screen.MainScreen(d_size[0], d_size[1], EM)

	SM = systems.SystemsManager(EM, stdscr, MS)

	#dungeon.Dungeon(d_size, EM)
	SM.event("new_dungeon", {})

	stdscr.nodelay(True)
	#stdscr.timeout(1)

	#EM.load("derp")

	SM.event("refresh", {})

	quit = 0
	while not quit:
		quit = SM.update_all()

	EM.save("derp")

	stdscr.refresh()


curses.wrapper(main)
