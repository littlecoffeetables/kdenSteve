#system_fov

#import collections

DIR = {"n":(0,-1), "e":(1,0), "w":(-1,0), "s":(0,1)}
ALGO = 0

class FOV(object):
	def __init__(self):
		#TODO: get rid of this
		self.__max_distance = None #was 9
		self.__to_reveal = set() #collections.deque()

	def update(self, EM, EQ):
		if not (EQ.can_get("move",0) or EQ.can_get("dead",0)
				or EQ.can_get("move_camera",0)):
			#if nothing moved or died, no one needs their fov
			# updated, I hope
			return
		#need_refresh = False
		#if EQ.can_get("move_camera", 0):
		#	need_refresh = True
		#if not need_refresh:
		#	i = 0
		#	while EQ.can_get("dead", i):
		#		event = EQ.get("dead", i)
		#		i += 1
		#		dead_pos = event["pos"]
		#		if EM.get_value(dead_pos, "in_fov", False):
		#			need_refresh = True
		#			break
		#if not need_refresh:
		#	return
		for e in EM.get_by_props(["vision", "p_pos"]):
			#this really needs optimisation - when does each entity
			# need to have its fov updated?
			self.calculate(EM, EQ, e)
	
	def calculate(self, EM, EQ, e):
		pos = EM.get_value(e, "p_pos")
		self.__max_distance = EM.get_value(e, "vision")
		# WIP: making this per-entity
		#EM.clear_prop("in_fov")
		self.__to_reveal = set()
		self.__to_reveal.add(pos)
		for dir_ in ("n","e","w","s",
				"nw","wn","ne","en","se","es","sw","ws"):
			self.recursive_calculate(EM, EQ, pos, dir_)
		if ALGO == 1:
			raise Exception("this needs updating")
		#	#actually reveal a plus shape around each flagged tile
		#	to_reveal = set()
		#	for pos in self.__to_reveal:
		#		for offset in ((0,0),(0,1),(0,-1),(1,0),(-1,0)):
		#			to_reveal.add((pos[0]+offset[0],
		#				pos[1]+offset[1]))
		#	for pos in to_reveal:
		#		self.reveal(EM, pos)
		elif ALGO == 0:
			if EM.has_prop(e, "camera"):
				for pos in self.__to_reveal:
					self.reveal(EM, pos)
			EM.set_value(e, "fov", self.__to_reveal)
		#self.__to_reveal.clear()
	
	def recursive_calculate(self, EM, EQ, pos, dir_, step=0):
		if step < self.__max_distance:
			step += 1
			offset = DIR[dir_[0]]
			new_pos = (pos[0]+offset[0], pos[1]+offset[1])
			#cycle dir to alternate steps
			add_current = True
			if self.visibility_blocking(EM, EQ, new_pos):
				# however, if the current tile is blocked
				# stop exploring the current direction
				if ALGO == 0:
					new_dir = dir_[1:]
				elif ALGO == 1:
					new_dir = ""
			else:
				new_dir = dir_[1:]+dir_[0]
				#new_dir = dir_[::-1]
				if ALGO == 1:
					self.__to_reveal.add(new_pos)
			if ALGO == 0:
				self.__to_reveal.add(new_pos)
			if len(new_dir) > 0:
				self.recursive_calculate(EM, EQ, new_pos,
						new_dir, step=step)
			if len(new_dir) > 1:
				#create a straight-going branch
				self.recursive_calculate(EM, EQ, new_pos,
						new_dir[0], step=step)


	def reveal(self, EM, pos):
		EM.set_value(pos, "revealed", True)
		#EM.set_value(pos, "in_fov", True)

	def visibility_blocking(self, EM, EQ, pos):
		if EM.get_value(pos, "opaque", False):
			return True
		for e in EM.get_at_pos(pos):
			if EM.get_value(e, "opaque", False):
				return True

