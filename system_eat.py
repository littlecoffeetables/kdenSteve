#system_eat

class Eat(object):
	def __init__(self):
		pass

	def update(self, EM, EQ):
		i = 0
		while EQ.can_get("chomp", i):
			event = EQ.get("chomp",i)
			i += 1
			eid = event["eid"]
			self.eat(EM, EQ, eid)

	def eat(self, EM, EQ, e):
		pos = EM.get_value(e, "p_pos")
		others = EM.get_at_pos(pos)
		if len(others) < 2:
			return False
		for other in others:
			if other == e:
				# can't eat yourself, stupid
				# well... at least not yet
				continue
			if not EM.has_props(other, ["edible"]):
				continue
			EQ.debug("aaargh")
			if EM.has_props(other, ["ai"]):
				# but what about bread that comes to life??
				# do you have to kill it first?
				continue
			heal = EM.get_value(other, "food_heal", 0)
			if heal > 0 and EM.has_prop(e, "health"):
				EM.edit_value(e, "health", add=heal)
			if EM.get_value(other, "blood", 0) > 100:
				EM.set_value(e, "bloody", True)
			EM.rm_entity(other)
			if EM.has_prop(e, "ap"):
				cost = 1 #TODO: softcode this
				EM.edit_value(e, "ap", add=-cost)
			#only eat one thing per keypress
			break
		else:
			return False
		return True
