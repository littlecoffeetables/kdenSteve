import key_config as kcfg

import curses

class InputHandler(object):
	def __init__(self, stdscr):
		#self.__EM = entityManager
		#self.__SM = systemsManager
		self.__stdscr = stdscr

	def update(self, EM, EQ):
		for e in EM.get_by_prop("controlled"):
			if EM.get_value(e, "ap", 1) < 1:
				#self.do_minimal_update(EM, EQ, e)
				continue
			self.do_update(EM, EQ, e)
			break
		else:
			self.do_minimal_update(EM, EQ)

	def do_minimal_update(self, EM, EQ):
		try:
			key = self.__stdscr.getkey()
		except curses.error:
			return
		if key in kcfg.quit:
			EQ.put("quit", {})

	def do_update(self, EM, EQ, i):
		try:
			key = self.__stdscr.getkey()
		except curses.error:
			return
		#EQ.put("debug", {"text":"input %s %i"%(key, i)})
		if key in kcfg.quit:
			EQ.put("quit", {})
		elif key in kcfg.up:
			self.pressed(EM,EQ,i,(0,-1))
		elif key in kcfg.down:
			self.pressed(EM,EQ,i,(0,1))
		elif key in kcfg.left:
			self.pressed(EM,EQ,i,(-1,0))
		elif key in kcfg.right:
			self.pressed(EM,EQ,i,(1,0))
		elif key in kcfg.wait:
			self.pressed(EM,EQ,i,(0,0))
		elif key in kcfg.chomp:
			EQ.put("chomp", {"eid":i})
		elif key in kcfg.attack:
			EM.set_value(i, "input_mode", "attack")
		elif key in kcfg.enter:
			#enter as in go inside, not the actual return/enter key
			EM.set_value(i, "input_mode", "enter")
		elif key in kcfg.exit:
			EM.set_value(i, "input_mode", "exit")
		elif key in kcfg.cancel:
			EM.rm_value(i, "input_mode", noerror=True)
		else:
			#curses.beep()
			pass
			EQ.debug("pressed %s" % key)
		EQ.put("refresh", {})

	def pressed(self, EM, EQ, i, dir_):
		mode = EM.get_value(i,"input_mode", None)
		if mode is None:
			EQ.put("move_or_attack", {"eid":i, "dir":dir_})
		elif mode == "attack":
			EQ.put("attack", {"eid":i, "dir":dir_})
		elif mode == "enter":
			EQ.put("enter", {"eid":i, "dir":dir_})
		elif mode == "exit":
			EQ.put("exit", {"eid":i, "dir":dir_})
		EM.rm_value(i, "input_mode", noerror=True)

