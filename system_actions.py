#system_actions
"""
Maybe this should be called system_turns or system_newturn instead
But basically this system does things that each entity do once per turn
 including having their ap reset, if they have ap
As opposed to once per tick
"""


class Actions(object):
	def __init__(self):
		pass

	def update(self, EM, EQ):
		EM.clear_prop("ap_updated")
		for e, ap in EM.get_prop("ap").items():
			if ap > 0:
				return
		self.new_turn(EM, EQ)
		EQ.put("new_turn", {})

	def new_turn(self, EM, EQ):
		# do not run AI, but tick the once-per-turn things
		EM.clear_prop("attacked_this_turn")
		EM.clear_prop("moved_this_turn")
		EM.clear_prop("dijkstra_this_turn")
		#for e in EM.get_by_props([]):
		#	EM.set_value(e, "stuck_counter", 0)
		for e in EM.get_by_props(["ap", "max_ap"]):
			ap = EM.get_value(e, "ap")
			max_ap = EM.get_value(e, "max_ap")
			EM.set_value(e, "ap", max_ap)
			EM.set_value(e, "stuck_counter", 0)

