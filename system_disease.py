#system_disease


class Disease(object):
	def __init__(self):
		pass

	#TODO: actual diseases, not just straight damage when weak

	def update(self, EM, EQ):
		if not EQ.can_get("new_turn", 0):
			return
		for e in EM.get_by_props(["health","biological"]):
			self.check_temperatures(EM, EQ, e)


	def check_temperatures(self, EM, EQ, e):
		tmp = EM.get_value(e, "temperature")
		normal_tmp = EM.get_value(e, "normal_temperature")
		if tmp is None or normal_tmp is None:
			return
		if tmp > normal_tmp + 3.0:
			dmg = int((tmp-normal_tmp)/3)
			EM.edit_value(e, "health", add=-dmg)
		elif tmp < normal_tmp - 5.0:
			EM.edit_value(e, "health", add=-1)
