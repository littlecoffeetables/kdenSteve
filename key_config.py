#import curses

quit	= ("Q",)

up	= ("KEY_UP",)
down	= ("KEY_DOWN",)
left	= ("KEY_LEFT",)
right	= ("KEY_RIGHT",)
wait	= (" ",)
chomp	= ("c",)
attack	= ("a",)
cancel	= ("\u001B",)
enter	= ("e",)
exit	= ("x",)
