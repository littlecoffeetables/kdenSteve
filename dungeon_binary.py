#dungeon_binarysplit

#import random
import collections

defaults = {
		#walls included, min,max
		"room_width": (5,12),
		"room_height": (5,12),
	}

def generate(EM, EQ, width, height, prefs={}):
	settings = collections.ChainMap(prefs, defaults)
	#rooms = []
	# now that the rng has been set to dungeon_rng we can assume it stays
	# as that the rest of this function
	vertical = EM.randint(0,1, "dungeon_rng")
	areas = recursive_split(0,0, width, height, vertical, settings)
	# TODO: place actual rooms within those areas and connect them up and
	# then create the actual entities


def recursive_split(x,y, width, height, vertical, settings):
	rooms = []
	if vertical:
		min_y = settings["room_height"][0]
		max_y = height - min_y
		comfy_min_y = settings["room_height"][1]
		comfy_max_y = height - comfy_min_y
		if comfy_min_y > comfy_max_y:
			rooms.append((x,y, width, height))
		else:
			split = EM.randint(min_y, max_y)
			rooms.extend(
				recursive_split(x,y,
					width, split,
					not vertical, settings)
			)
			rooms.extend(
				recursive_split(x,y+split,
					width, height-split,
					not vertical, settings)
			)
	else:
		min_x = settings["room_width"][0]
		max_x = width - min_x
		comfy_min_y = settins["room_width"][1]
		comfy_max_y = width - comfy_min_y
		if comfy_min_x > comfy_max_x:
			rooms.append((x,y, width, height))
		else:
			split = EM.randint(min_y, max_y)
			rooms.extend(
				recursive_split(x,y,
					split, height,
					not vertical, settings)
			)
			rooms.extend(
				recursive_split(x+split,y,
					width-split, height,
					not vertical, settings)
			)
	return rooms

