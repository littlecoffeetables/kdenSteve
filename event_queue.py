import collections


class EventQueue(object):
	# do I want to have a way to cancel all events this turn
	# for in case an illegal event was queued?
	def __init__(self):
		self.__queues = collections.defaultdict(collections.deque)
		self.__scheduled = list()

	def put(self, evtype, item):
		self.__queues[evtype].append(item)

	def debug(self, note):
		self.put("debug", {"text":str(note)})

	def get(self, evtype, index):
		item = self.__queues[evtype][index]
		return item

	def pop(self, evtype):
		return self.__queues[evtype].pop()

	def can_get(self, evtype, index):
		return index < len(self.__queues[evtype])

	def clear(self, evtype):
		if evtype == "all":
			self.__queues.clear()
		else:
			self.__queues[evtype].clear()

	def schedule(self, evtype, item):
		self.__scheduled.append((evtype, item))
	
	def run_scheduled(self):
		for evtype, item in self.__scheduled:
			self.put(evtype, item)
		self.__scheduled.clear()
