import curses

class Renderer(object):
	def __init__(self, stdscr, mainscreen):
		self.__stdscr = stdscr
		self.__mscr = mainscreen
		self.__other_screens = [] #TODO
		self.__bot_panel_h = 1
		#TODO: get rid of this
		self.__player_id = None

	def update(self, EM, EQ):
		if not EQ.can_get("refresh", 0):
			return
		if self.__player_id == None:
			self.__player_id = \
					tuple(EM.get_by_prop("controlled"))[0]
		self.__mscr.update(EM, EQ,
				(0,0, curses.COLS-1,
				 curses.LINES-1-self.__bot_panel_h),
				self.__player_id)
		
		# BOTTOM BAR
		
		#health
		hp = EM.get_value(self.__player_id, "health", "N/A")
		self.__stdscr.move(curses.LINES-self.__bot_panel_h, 0)
		self.__stdscr.addch(curses.ACS_DIAMOND, self.get_color_pair(
			curses.COLOR_RED, curses.COLOR_BLACK))
		hp_attrs = 0
		if type(hp) == int and hp < 10:
			hp_attrs |= curses.A_BLINK
			hp_attrs |= self.get_color_pair(
					curses.COLOR_RED, curses.COLOR_BLACK)
		self.__stdscr.addstr("{0:>3} ".format(hp), hp_attrs)
		
		#action points
		ap = EM.get_value(self.__player_id, "ap")
		max_ap = EM.get_value(self.__player_id, "max_ap")
		if ap is None:
			ap_str = "N/A "
		else:
			ap_str = "{0:>1}/{1:>1} ".format(ap, max_ap)
		apc = self.get_color_pair(curses.COLOR_BLUE, curses.COLOR_BLACK)
		self.__stdscr.addch(curses.ACS_DARROW, apc)
		self.__stdscr.addstr(ap_str, apc)
		
		#temperature
		tmp = EM.get_value(self.__player_id, "temperature")
		self.__stdscr.addch(102, self.get_color_pair(
			curses.COLOR_WHITE, curses.COLOR_BLACK)
			 | curses.A_ALTCHARSET)
		if tmp is None:
			tmp_str = "N/A "
		else:
			tmp_str = "{0:>4} ".format(round(tmp, 1))
		tmp_attrs = self.get_tmp_attrs(tmp)
		self.__stdscr.addstr(tmp_str, tmp_attrs)

		# TOP BAR
		
		imode = EM.get_value(self.__player_id, "input_mode", None)
		if imode == "attack":
			self.__stdscr.addstr(0,0, "Attack", self.get_color_pair(
				curses.COLOR_BLUE, curses.COLOR_WHITE))
		elif imode == "enter":
			self.__stdscr.addstr(0,0, "Enter", self.get_color_pair(
				curses.COLOR_BLUE, curses.COLOR_WHITE))
		elif imode == "exit":
			self.__stdscr.addstr(0,0, "Exit", self.get_color_pair(
				curses.COLOR_BLUE, curses.COLOR_WHITE))

		curses.doupdate()

	#def move_camera(self, to):
	#	self.__mscr.update_coords(to)
	
	def get_color_pair(self, fg, bg):
		return curses.color_pair(1 + 8 * fg + bg)


	def get_tmp_attrs(self, tmp):
		tmp_attrs = 0
		if 35.0 <= tmp < 36.5:
			#slightly below normal
			tmp_attrs |= self.get_color_pair(
				curses.COLOR_CYAN, curses.COLOR_BLACK)
		if tmp < 35.0:
			#hypothermia
			tmp_attrs |= self.get_color_pair(
				curses.COLOR_CYAN, curses.COLOR_YELLOW)
		if 37.5 < tmp <= 38.3:
			#slightly above normal
			tmp_attrs |= self.get_color_pair(
				curses.COLOR_MAGENTA, curses.COLOR_BLACK)
		if tmp > 38.3:
			#hypertermia
			tmp_attrs |= self.get_color_pair(
				curses.COLOR_RED, curses.COLOR_YELLOW)
		if tmp < 32.0 or tmp > 40.0:
			#severe
			tmp_attrs |= curses.A_BLINK
		return tmp_attrs

