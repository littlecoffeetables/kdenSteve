import event_queue
import system_input
import system_movement
import system_render
import system_logger
import system_attack
import system_corpses
import system_ai
import system_actions
import system_dijkstra
import system_eat
import system_dungeon
import system_fov
import system_bathing
import system_temperature
import system_disease
import system_enter

import collections


class SystemsManager(object):
	def __init__(self, entityManager, stdscr, MS):
		self.__EM = entityManager
		self.__systems = [
				system_dungeon.Dungeon(),
				
				system_actions.Actions(),
				system_input.InputHandler(stdscr),
				system_ai.AI(),
				
				system_attack.Attacks(),
				system_movement.Movement(),
				system_enter.Enter(),
				
				system_eat.Eat(),
				system_corpses.Corpses(),
				system_bathing.Bathing(),
				system_temperature.Temperature(),
				system_disease.Disease(),

				system_fov.FOV(),
				system_dijkstra.Dijkstra(),
				system_render.Renderer(stdscr, MS),
				system_logger.Logger(),
				]
		self.__EQ = event_queue.EventQueue()
		self.__EM.debug_set_EQ(self.__EQ)

	def update_all(self):
		self.__EQ.run_scheduled()
		for system in self.__systems:
			system.update(self.__EM, self.__EQ)
		if self.__EQ.can_get("quit",0):
			return 1
		self.__EQ.clear("all")
		return 0

	def event(self, evtype, item):
		self.__EQ.put(evtype, item)

