import dungeon_simple

import curses #for colours

class Dungeon(object):
	def __init__(self):
		pass

	def update(self, EM, EQ):
		if EQ.can_get("new_dungeon",0):
			self.generate(EM, EQ)
			player = tuple(EM.get_by_prop("camera"))[0]
			player_pos = EM.get_value(player, "p_pos")
			EQ.put("move_camera", {"to":player_pos})

	def generate(self, EM, EQ):
		dungeon_simple.generate(EM, EQ, 100, 100)


"""
		#self.__size = size
		#self.__EM = EM
		for x in range(self.__size[0]):
			for y in range(self.__size[1]):
				pos = (x, y)

				material = "floor"
				if (x == 0 or y == 0
				 or x == self.__size[0] - 1
				 or y == self.__size[1] - 1
				 or x == y == 5):
					material = "wall"
				self.create_tile(pos, material)
		self.populate()"""

"""
	def populate(self):
		#place monsters and shiz
		self.__EM.quick_entity("orc", (20,10))
		self.__EM.quick_entity("orc", (28, 7))
		self.__EM.quick_entity("player", (3,7))

	def create_tile(self, pos, mat):
		if mat == "wall":
			self.__EM.create_tile(pos, {"material":mat,
				"r_char":"#", "r_prio":1000, "physical":True,
				"r_colour": curses.COLOR_BLACK,
				"bg_colour": curses.COLOR_WHITE,
				})
		elif mat == "floor":
			self.__EM.create_tile(pos, {"material":mat,
				"r_char":".", "r_prio":0,
				"r_colour": curses.COLOR_BLACK,
				"bg_colour": curses.COLOR_WHITE,
				})
"""

